<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fotos".
 *
 * @property int $id
 * @property string|null $ruta
 * @property int|null $id_prendas
 *
 * @property Prendas $prendas
 */
class Fotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fotos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_prendas'], 'integer'],
            [['ruta'], 'string', 'max' => 100],
            [['id_prendas'], 'exist', 'skipOnError' => true, 'targetClass' => Prendas::className(), 'targetAttribute' => ['id_prendas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ruta' => 'Ruta',
            'id_prendas' => 'Id Prendas',
        ];
    }

    /**
     * Gets query for [[Prendas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrendas()
    {
        return $this->hasOne(Prendas::className(), ['id' => 'id_prendas']);
    }
}
