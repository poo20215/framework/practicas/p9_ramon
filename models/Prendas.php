<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prendas".
 *
 * @property int $id
 * @property string|null $titulo
 * @property string|null $referencia
 * @property float|null $precio
 * @property float|null $descuento
 * @property string|null $foto
 * @property int|null $portada
 * @property int|null $oferta
 * @property int|null $id_categorias
 *
 * @property Caracteristicas[] $caracteristicas
 * @property Categorias $categorias
 * @property Fotos[] $fotos
 */
class Prendas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prendas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio', 'descuento'], 'number'],
            [['portada', 'oferta', 'id_categorias'], 'integer'],
            [['titulo', 'referencia', 'foto'], 'string', 'max' => 100],
            [['id_categorias'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::className(), 'targetAttribute' => ['id_categorias' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'referencia' => 'Referencia',
            'precio' => 'Precio',
            'descuento' => 'Descuento',
            'foto' => 'Foto',
            'portada' => 'Portada',
            'oferta' => 'Oferta',
            'id_categorias' => 'Id Categorias',
        ];
    }

    /**
     * Gets query for [[Caracteristicas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCaracteristicas()
    {
        return $this->hasMany(Caracteristicas::className(), ['id_prendas' => 'id']);
    }

    /**
     * Gets query for [[Categorias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategorias()
    {
        return $this->hasOne(Categorias::className(), ['id' => 'id_categorias']);
    }

    /**
     * Gets query for [[Fotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFotos()
    {
        return $this->hasMany(Fotos::className(), ['id_prendas' => 'id']);
    }
}
