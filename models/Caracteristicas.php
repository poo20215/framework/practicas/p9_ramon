<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "caracteristicas".
 *
 * @property int $id
 * @property string|null $caracteristica
 * @property int|null $id_prendas
 *
 * @property Prendas $prendas
 */
class Caracteristicas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'caracteristicas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_prendas'], 'integer'],
            [['caracteristica'], 'string', 'max' => 255],
            [['caracteristica', 'id_prendas'], 'unique', 'targetAttribute' => ['caracteristica', 'id_prendas']],
            [['id_prendas'], 'exist', 'skipOnError' => true, 'targetClass' => Prendas::className(), 'targetAttribute' => ['id_prendas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'caracteristica' => 'Caracteristica',
            'id_prendas' => 'Id Prendas',
        ];
    }

    /**
     * Gets query for [[Prendas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrendas()
    {
        return $this->hasOne(Prendas::className(), ['id' => 'id_prendas']);
    }
}
