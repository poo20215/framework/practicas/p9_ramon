<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use app\models\Prendas;
use app\models\Categorias;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $fotos= Prendas::find()->where(['portada'=>1])->all();
        foreach($fotos as $foto)
        {
            $carousel[]=[
            'content' => Html::img("@web/imgs/$foto->foto",["height"=>300,"width"=>200]),
            'caption' =>"<p style=color:blue class=bg-white>{$foto->precio}€</p>" . Html::a("Ver",["site/verprenda", 'id' => $foto->id], ['class' => 'btn btn-primary']),
        ];
        }
        
        $dataProvider = new ActiveDataProvider([
        'query' => Prendas::find()->where(['portada'=>1])]);
        $dataProvider1 = new ActiveDataProvider([
        'query' => Prendas::find()->where(['oferta'=>1])]);
        return $this->render('index',["carousel" => $carousel, "dataProvider" => $dataProvider, "dataProvider1" => $dataProvider1]);
    }
    
    public function actionVercategoria($tipo,$subtipo)
    {
        $categoria=Categorias::find()->where(['tipo'=>$tipo,'subtipo'=>$subtipo])->one();
        $dataProvider = new ActiveDataProvider([
        'query' => $categoria->getPrendas()]);
        return $this->render('vercategoria',["tipo" => $tipo, "subtipo" => $subtipo, "dataProvider" => $dataProvider]);
    }
    
    public function actionVercategoriatipo($tipo)
    {
        $categorias= Categorias::find()->where(['tipo'=>$tipo])->all();
        $idCategorias=[];
        foreach($categorias as $categoria)
        {
            $idCategorias[]=$categoria->id;
        }
        $dataProvider = new ActiveDataProvider([
        'query' => Prendas::find()->where(['id_categorias'=>$idCategorias])]);
        return $this->render('vercategoria',["tipo" => $tipo, "subtipo" => "todo", "dataProvider" => $dataProvider]);
    }
    
    /*
     * Esta accion muestra los datos de cada prenda
     */
    public function actionVerprenda($id)
    {
        $prenda= Prendas::findOne($id);
        return $this->render('verprenda',["prenda" => $prenda]);  
    }
    
    public function actionOfertas()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Prendas::find()->where(['oferta'=>1])]);
        return $this->render('ofertas',["dataProvider" => $dataProvider, "migas" => true]); 
    }
    
    public function actionDestacados()
    {
        $categorias= Categorias::find()->select('tipo')->distinct()->all();
        return $this->render('destacados',["categorias" => $categorias]); 
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
