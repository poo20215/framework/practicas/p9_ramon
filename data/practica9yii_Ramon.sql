-- MySQL dump 10.13  Distrib 5.7.36, for Win64 (x86_64)
--
-- Host: localhost    Database: practica9yii_Ramon
-- ------------------------------------------------------
-- Server version	5.7.36-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `caracteristicas`
--

DROP TABLE IF EXISTS `caracteristicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caracteristicas` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `caracteristica` varchar(255) DEFAULT NULL,
  `id_prendas` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `caracteristica` (`caracteristica`,`id_prendas`),
  KEY `fk_caracteristicas_prendas` (`id_prendas`),
  CONSTRAINT `fk_caracteristicas_prendas` FOREIGN KEY (`id_prendas`) REFERENCES `prendas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caracteristicas`
--

LOCK TABLES `caracteristicas` WRITE;
/*!40000 ALTER TABLE `caracteristicas` DISABLE KEYS */;
INSERT INTO `caracteristicas` VALUES (67,'Abrigo acolchado termosellado sin costuras con capucha y cuello subido',22),(37,'Abrigo confeccionado con tejido en mezcla de lana',11),(41,'Abrigo de cuello amplio tipo bufanda y manga larga acabada en vuelta',12),(34,'Abrigo de cuello solapa con manga larga',10),(59,'Acabado puño en bajo',19),(33,'Acabados en rib',9),(76,'Aplique texto a contraste en pernera',24),(45,'Bajo acabado en cremallera lateral',14),(49,'Bajo acabado en línea evasé',16),(27,'Bajo ajustable con elástico en laterales',7),(72,'Bolsillo combinado tipo plastrón en pechera',23),(7,'Bolsillo de solapa en pecho',2),(26,'Bolsillo frontal con cierre de cremallera',7),(32,'Bolsillo frontal tipo canguro',9),(65,'Bolsillo tipo plastrón en pechera',21),(75,'Bolsillos combinados en delantero con detalle vivo tapeta',24),(4,'Bolsillos con cierre de cremallera en cadera y detalle de bolsillo interior',1),(12,'Bolsillos de vivo en cadera y detalle de bolsillo interior',3),(15,'Bolsillos de vivo en delantero y espalda',4),(39,'Bolsillos delanteros con solapa',11),(35,'Bolsillos delanteros de vivo',10),(57,'Bolsillos en delantero',19),(18,'Bolsillos frontales y de vivo en espalda',5),(23,'Bolsillos laterales en cadera',6),(3,'Bordado en pecho de texto',1),(60,'Camiseta con cuello subido y manga larga',20),(71,'Camiseta cuello redondo y manga larga acabado puño',23),(1,'Cazadora acolchada en su interior',1),(10,'Cazadora con cuello solapa en tejido de pana combinado a contraste',3),(6,'Cazadora de cuello solapa y manga larga acabada en puño con botón',2),(64,'Cierre botonadura presión frontal',21),(69,'Cierre cremallera frontal inyectada y bolsillos delanteros',22),(61,'Cierre cremallera termosellada en canesú',20),(55,'Cierre en espalda con cremallera oculta en costura',18),(20,'Cierre frontal con cremallera y doble botonadura',5),(36,'Cierre frontal cruzado con botones',10),(40,'Cierre frontal cruzado con botones metálicos con relieve',11),(9,'Cierre frontal de botonadura',2),(5,'Cierre frontal de cremallera',1),(13,'Cierre frontal de cremallera',3),(46,'Cierre frontal oculto con cremallera y ganchos metálicos',14),(43,'Cierre lateral con cremallera oculta en costura',13),(50,'Cierre lateral con cremallera oculta en costura',16),(53,'Cierre lateral con cremallera oculta en costura',17),(22,'Cintura elástica',6),(2,'Cuello solapa y manga larga acabada en puño elástico',1),(38,'Cuello subido y manga larga con hombros marcados',11),(77,'Detalle cortes y acabado puño en bajo',24),(52,'Detalle de aplicación lentejuelas combinadas a tono',17),(16,'Detalle de banda lateral combinada a tono',4),(19,'Detalle de texto bordado en bolsillo',5),(8,'Detalle de texto frontal bordado',2),(24,'Detalle de vivo combinado a contraste en laterales y bajo',6),(58,'Detalle pespuntes a tono',19),(66,'Detalle pespuntes a tono',21),(29,'Estampaciones combinadas a contraste en delantero y espalda de spalding russell branbs llc',8),(31,'Estampaciones combinadas a contraste en delantero y espalda de spalding russell brands llc',9),(62,'Estampado all over',20),(73,'Estampado fluor delantero y en espalda con detalle en relieve',23),(51,'Falda corta de tiro alto',17),(54,'Falda corta estampada de tiro alto',18),(48,'Falda midi estampada de tiro alto',16),(68,'Manga larga',22),(11,'Manga larga acabada en puño elástico',3),(56,'Pantalón acolchado con cinturilla elástica',19),(74,'Pantalón con cinturilla elástica y cordones ajustables frontal',24),(14,'Pantalón de cintura elástica ajustable con cordón',4),(44,'Pantalón de tiro alto confeccionado con tejido en mezcla de lana',14),(47,'Pantalón de tiro alto y cintura elástica con pernera recta',15),(21,'Pantalón fluido confeccionado en tejido satinado',6),(42,'Pantalón fluido de tiro alto con pierna ancha',13),(17,'Pantalón straight fit',5),(63,'Sobrecamisa acolchada con cuello solapas y manga larga',21),(70,'Su tejido sin costuras lo hace más resistente al viento y a la nieve, manteniendo más el calor, a su vez es más ligero y cálido',22),(28,'Sudadera con cuello redondo y manga larga',8),(30,'Sudadera de cuello con capucha ajustable y manga larga',9),(25,'Sudadera de cuello subido con cierre frontal de cremallera y manga larga',7);
/*!40000 ALTER TABLE `caracteristicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `tipo` enum('hombre','mujer','niño') DEFAULT NULL,
  `subtipo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tipo` (`tipo`,`subtipo`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (1,'hombre','cazadoras'),(2,'hombre','pantalones'),(3,'hombre','sudaderas'),(4,'mujer','abrigos'),(6,'mujer','faldas'),(5,'mujer','pantalones'),(7,'niño','todo');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fotos`
--

DROP TABLE IF EXISTS `fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fotos` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `ruta` varchar(100) DEFAULT NULL,
  `id_prendas` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_fotos_prendas` (`id_prendas`),
  CONSTRAINT `fk_fotos_prendas` FOREIGN KEY (`id_prendas`) REFERENCES `prendas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fotos`
--

LOCK TABLES `fotos` WRITE;
/*!40000 ALTER TABLE `fotos` DISABLE KEYS */;
INSERT INTO `fotos` VALUES (1,'cazadora_bordado1.webp',1),(2,'cazadora_bordado2.webp',1),(3,'cazadora_bordado3.webp',1),(4,'cazadora_bordado4.webp',1),(5,'cazadora_pana1.webp',2),(6,'cazadora_pana2.webp',2),(7,'cazadora_pana3.webp',2),(8,'cazadora_pana4.webp',2),(9,'cazadora_acolchada1.webp',3),(10,'cazadora_acolchada2.webp',3),(11,'cazadora_acolchada3.webp',3),(12,'cazadora_acolchada4.webp',3),(13,'pantalon_soft1.webp',4),(14,'pantalon_soft2.webp',4),(15,'pantalon_soft3.webp',4),(16,'pantalon_soft4.webp',4),(17,'pantalon_pana1.webp',5),(18,'pantalon_pana2.webp',5),(19,'pantalon_pana3.webp',5),(20,'pantalon_pana4.webp',5),(21,'pantalon_pijama1.webp',6),(22,'pantalon_pijama2.webp',6),(23,'pantalon_pijama3.webp',6),(24,'pantalon_pijama4.webp',6),(25,'sudadera_polar1.webp',7),(26,'sudadera_polar2.webp',7),(27,'sudadera_polar3.webp',7),(28,'sudadera_polar4.webp',7),(29,'sudadera_estampado1.webp',8),(30,'sudadera_estampado2.webp',8),(31,'sudadera_estampado3.webp',8),(32,'sudadera_estampado4.webp',8),(33,'sudadera_capucha1.webp',9),(34,'sudadera_capucha2.webp',9),(35,'sudadera_capucha3.webp',9),(36,'sudadera_capucha4.webp',9),(37,'abrigo_oversize1.webp',10),(38,'abrigo_oversize2.webp',10),(39,'abrigo_oversize3.webp',10),(40,'abrigo_oversize4.webp',10),(41,'abrigo_patagallo1.webp',11),(42,'abrigo_patagallo2.webp',11),(43,'abrigo_patagallo3.webp',11),(44,'abrigo_patagallo4.webp',11),(45,'abrigo_punto1.webp',12),(46,'abrigo_punto2.webp',12),(47,'abrigo_punto3.webp',12),(48,'abrigo_punto4.webp',12),(49,'pantalon_fluido1.webp',13),(50,'pantalon_fluido2.webp',13),(51,'pantalon_fluido3.webp',13),(52,'pantalon_fluido4.webp',13),(53,'pantalon_rayas1.webp',14),(54,'pantalon_rayas2.webp',14),(55,'pantalon_rayas3.webp',14),(56,'pantalon_rayas4.webp',14),(57,'pantalon_satinado1.webp',15),(58,'pantalon_satinado2.webp',15),(59,'pantalon_satinado3.webp',15),(60,'pantalon_satinado4.webp',15),(61,'falda_estampada1.webp',16),(62,'falda_estampada2.webp',16),(63,'falda_estampada3.webp',16),(64,'falda_estampada4.webp',16),(65,'falda_lentejuelas1.webp',17),(66,'falda_lentejuelas2.webp',17),(67,'falda_lentejuelas3.webp',17),(68,'falda_lentejuelas4.webp',17),(69,'falda_corta1.webp',18),(70,'falda_corta2.webp',18),(71,'falda_corta3.webp',18),(72,'falda_corta4.webp',18),(73,'pantalon_nina1.webp',19),(74,'pantalon_nina2.webp',19),(75,'pantalon_nina3.webp',19),(76,'camiseta_nina1.webp',20),(77,'camiseta_nina2.webp',20),(78,'camiseta_nina3.webp',20),(79,'sobrecamisa_nina1.webp',21),(80,'sobrecamisa_nina2.webp',21),(81,'sobrecamisa_nina3.webp',21),(82,'abrigo_nino1.webp',22),(83,'abrigo_nino2.webp',22),(84,'abrigo_nino3.webp',22),(85,'camiseta_nino1.webp',23),(86,'camiseta_nino2.webp',23),(87,'camiseta_nino3.webp',23),(88,'pantalon_nino1.webp',24),(89,'pantalon_nino2.webp',24),(90,'pantalon_nino3.webp',24);
/*!40000 ALTER TABLE `fotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prendas`
--

DROP TABLE IF EXISTS `prendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prendas` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) DEFAULT NULL,
  `referencia` varchar(100) DEFAULT NULL,
  `precio` float(10,2) DEFAULT NULL,
  `descuento` float(10,2) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `portada` tinyint(1) DEFAULT NULL,
  `oferta` tinyint(1) DEFAULT NULL,
  `id_categorias` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_prendas_categorias` (`id_categorias`),
  CONSTRAINT `fk_prendas_categorias` FOREIGN KEY (`id_categorias`) REFERENCES `categorias` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prendas`
--

LOCK TABLES `prendas` WRITE;
/*!40000 ALTER TABLE `prendas` DISABLE KEYS */;
INSERT INTO `prendas` VALUES (1,'CAZADORA BORDADO','GIU64',49.95,39.95,'cazadora_bordado1.webp',1,1,1),(2,'CAZADORA PANA','G7FR3',39.95,NULL,'cazadora_pana1.webp',0,0,1),(3,'CAZADORA ACOLCHADA','JK97P',49.95,NULL,'cazadora_acolchada1.webp',1,0,1),(4,'PANTALÓN SOFT','56HE2',29.95,19.95,'pantalon_soft1.webp',0,1,2),(5,'PANTALÓN PANA','HJ5H7',29.95,NULL,'pantalon_pana1.webp',1,0,2),(6,'PANTALÓN PIJAMA','67JPI',29.95,19.95,'pantalon_pijama1.webp',0,1,2),(7,'SUDADERA POLAR','Y7JK5',49.95,NULL,'sudadera_polar1.webp',1,0,3),(8,'SUDADERA ESTAMPADO','AD329',29.95,24.95,'sudadera_estampado1.webp',1,1,3),(9,'SUDADERA CAPUCHA','8KUT4',35.95,NULL,'sudadera_capucha1.webp',0,0,3),(10,'ABRIGO OVERSIZE','HJ75Y',59.95,NULL,'abrigo_oversize1.webp',1,0,4),(11,'ABRIGO PATA DE GALLO','JK6L9',69.95,59.95,'abrigo_patagallo1.webp',1,1,4),(12,'ABRIGO PUNTO','K8J0I',49.95,NULL,'abrigo_punto1.webp',1,0,4),(13,'PANTALÓN FLUIDO','GKT54',49.95,NULL,'pantalon_fluido1.webp',1,0,5),(14,'PANTALÓN RAYAS','NK98D',69.95,59.95,'pantalon_rayas1.webp',0,1,5),(15,'PANTALÓN SATINADO','N9GH2',22.95,NULL,'pantalon_satinado1.webp',1,0,5),(16,'FALDA ESTAMPADA','KY5S2',29.95,24.95,'falda_estampada1.webp',1,1,6),(17,'FALDA LENTEJUELAS','MG09L',25.95,NULL,'falda_lentejuelas1.webp',1,0,6),(18,'FALDA CORTA','BJ96R',25.95,NULL,'falda_corta1.webp',0,0,6),(19,'PANTALÓN NIÑA','BU75R',25.95,19.95,'pantalon_nina1.webp',1,1,7),(20,'CAMISETA NIÑA','B8JK4',15.95,NULL,'camiseta_nina1.webp',0,0,7),(21,'SOBRECAMISA NIÑA','HL03W',29.95,NULL,'sobrecamisa_nina1.webp',1,0,7),(22,'ABRIGO NIÑO','KM80E',25.95,19.95,'abrigo_nino1.webp',1,1,7),(23,'CAMISETA NIÑO','NLF63',12.95,NULL,'camiseta_nino1.webp',0,0,7),(24,'PANTALÓN NIÑO','LY68E',19.95,NULL,'pantalon_nino1.webp',0,0,7);
/*!40000 ALTER TABLE `prendas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-09  9:58:04
