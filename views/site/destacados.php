<?php
use app\models\Categorias;
use app\models\Prendas;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;

foreach($categorias as $categoria)
{
    $categorias2=Categorias::find()->where(["tipo"=>$categoria->tipo])->all();
    $idCategorias=[];
        foreach($categorias2 as $categoria2)
        {
            $idCategorias[]=$categoria2->id;
        }
    $dataProvider = new ActiveDataProvider([
        'query' => Prendas::find()->where(['portada'=>1,'id_categorias'=>$idCategorias])]);
    
?>
<h1><?= strtoupper($categoria->tipo) ?></h1>

<?php
    
    echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_listar',
    "itemOptions" => [
        'class' => 'col-lg-4 border m-2 p-2',
    ],
    "options" => [
        'class' => 'row',
    ],
    'layout'=>"{items}"

    ]);
    
    echo "<br>";
}
