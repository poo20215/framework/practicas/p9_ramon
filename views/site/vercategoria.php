<?php
use yii\widgets\Breadcrumbs;
use yii\widgets\ListView;

$this->params['breadcrumbs'][] = ['label' => $tipo, 'url' => ['site/vercategoriatipo', 'tipo' => $tipo]];
$this->params['breadcrumbs'][] = $subtipo;

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_listar',
    "itemOptions" => [
        'class' => 'col-lg-4 border m-2 p-2',
    ],
    "options" => [
        'class' => 'row',
    ],
    'layout'=>"{items}"

    ]);