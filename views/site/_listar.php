<?php
use yii\helpers\Html;


echo Html::img("@web/imgs/$model->foto",["height"=>300,"width"=>200]);
echo "<br>";
echo "{$model->precio}€";
echo "<br>";
echo Html::a("Ver",["site/verprenda", 'id' => $model->id], ['class' => 'btn btn-primary']);
