<?php

use yii\bootstrap4\Carousel;
use yii\helpers\Html;
use yii\widgets\ListView;


$this->title = 'Alpe Shop';
?>

<h2 style="float: right">Moda</h2>
<br><br>
<?php
    echo Carousel::widget([
        'items' => $carousel,
        'options'=>[
            "style" => ["height"=>"300px","width"=>"200px","margin"=>"auto"]
        ],
    ]);
    
echo "<br><br>";   

    echo $this->render('ofertas',["dataProvider" => $dataProvider1, "migas" => false]); 
    
    echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_listar',
    "itemOptions" => [
        'class' => 'col-lg-4 border m-2 p-2',
    ],
    "options" => [
        'class' => 'row',
    ],
    'layout'=>"{items}"

    ]);
    
?>





