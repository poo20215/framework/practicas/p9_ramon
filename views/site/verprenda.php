<?php
use yii\widgets\Breadcrumbs;
use newerton\fancybox\FancyBox;
use yii\helpers\Html;

$categoria=$prenda->getCategorias()->one();
//$categoria=$prenda->categorias; //Hace lo mismo que la fila anterior
$fotos=$prenda->getFotos()->all();
$caracteristicas=$prenda->getCaracteristicas()->all();

$this->params['breadcrumbs'][] = ['label' => $categoria->tipo, 'url' => ['site/vercategoriatipo', 'tipo' => $categoria->tipo]];
$this->params['breadcrumbs'][] = ['label' => $categoria->subtipo, 'url' => ['/site/vercategoria','tipo'=>$categoria->tipo,"subtipo"=>$categoria->subtipo]];
$this->params['breadcrumbs'][] = $prenda->titulo;

echo FancyBox::widget([
    'target' => 'a[rel=fancybox]',
    'helpers' => true,
    'mouse' => true,
    'config' => [
        'maxWidth' => '90%',
        'maxHeight' => '90%',
        'playSpeed' => 7000,
        'padding' => 0,
        'fitToView' => false,
        'width' => '70%',
        'height' => '70%',
        'autoSize' => false,
        'closeClick' => false,
        'openEffect' => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect' => 'elastic',
        'nextEffect' => 'elastic',
        'closeBtn' => false,
        'openOpacity' => true,
        'helpers' => [
            'title' => ['type' => 'float'],
            'buttons' => [],
            'thumbs' => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)',
                ]
            ]
        ],
    ]
]);

foreach ($fotos as $foto) 
{
        echo Html::a(Html::img("@web/imgs/{$foto->ruta}",["height"=>300,"width"=>200,"style" => "margin:5px;"]), "@web/imgs/{$foto->ruta}", ['rel' => 'fancybox']);    
}
?>
<br><br>
<h3><?= $prenda->titulo ?></h3>
<h4><?= $prenda->referencia ?></h4>

<ul>
<?php
foreach($caracteristicas as $caracteristica)
{
?>
    <li><?= $caracteristica->caracteristica ?></li>
<?php    
}
?>
</ul>


<?php
echo "PRECIO: " . $prenda->precio . "€<br>";
if($prenda->descuento!=null)
{
    echo "DESCUENTO: " . $prenda->descuento . "€<br>";
}

?>