<?php
use yii\helpers\Html;
use dominus77\owlcarousel2\WrapCarousel;
use yii\widgets\ListView;

if($migas)
{
    $this->params['breadcrumbs'][] = "Ofertas";
}
?>

<h2>Ofertas</h2>
<br>
<?php WrapCarousel::begin([
    //'theme' => \dominus77\owlcarousel2\Carousel::THEME_GREEN, // THEME_DEFAULT, THEME_GREEN
    //'tag' => 'div', // container tag name, default div
    //'containerOptions' => [/* ... */], // container html options
    'clientOptions' => [
        'autoplay' => true,
        'loop' => true,
        'margin' => 10,
        'nav' => true,
        'responsive' => [
            0 => [
                'items' => 1,
            ],
            600 => [
                'items' => 3,
            ],
            1000 => [
                'items' => 5,
            ],
        ],
    ],
       
]); 

    echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_listar',
    "itemOptions" => [
        'class' => 'col-lg-12',
    ],
    "options" => [
        'class' => 'row',
        'tag' => false //Necesario para que no me agrupe todo el ListView dentro de un <div>
    ],
    'layout'=>"{items}"

    ]);

WrapCarousel::end()
        
?>